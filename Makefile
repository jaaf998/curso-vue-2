#!/bin/bash

DOCKER_FRONT = frontend-vue
OS := $(shell uname)

ifeq ($(OS),Darwin)
	UID = $(shell id -u)
else ifeq ($(OS),Linux)
	UID = $(shell id -u)
else
	UID = 1000
endif

help: ## Show this help message
	@echo 'usage: make [target]'
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

build: ## Rebuilds all the containers
	U_ID=${UID} docker-compose build

run: ## Start the containers
	U_ID=${UID} docker-compose up -d

stop: ## Stop the containers
	U_ID=${UID} docker-compose stop

restart: ## Restart the containers
	$(MAKE) stop && $(MAKE) run

logs: ## Tails the Symfony dev log
	U_ID=${UID} docker logs -f ${DOCKER_FRONT}

node-install: ## Installs node dependencies
	U_ID=${UID} docker exec --user ${UID} -t ${DOCKER_FRONT} npm install -g @vue/cli